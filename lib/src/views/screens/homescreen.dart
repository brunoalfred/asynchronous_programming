import 'package:flutter/material.dart';
import 'package:asynchronous_programming/src/views/components/expendable_fab.dart';

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return ExampleExpandableFab();
  }
}
